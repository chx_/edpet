<?php

namespace Drupal\Tests\edpet\Kernel;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Serialization\Yaml;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\token\Kernel\KernelTestBase;

class EdpetTest extends KernelTestBase {

  /**
   * @var array
   */
  protected $aliases = [];

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'edpet',
    'field',
    'node',
    'system',
    'user',
  ];

  protected $fieldName;

  protected $contentType;

  protected $paragraphType;


  protected function setUp() {
    parent::setUp();
    $this->installConfig('system');
    $this->installSchema('system', 'sequences');
    $this->installSchema('node', 'node_access');
    $this->installEntitySchema('node');
    $this->installEntitySchema('user');
  }

  public function testEdpet() {
    $fieldName = strtolower($this->randomMachineName(8));
    $edpetName = 'edpet.form.node.default';
    $this->config($edpetName)
      ->set('content.' . $fieldName, [
        'type' => 'options_buttons',
      ])
      ->save();
    $id = strtolower($this->randomMachineName(8));
    NodeType::create(['type' => $id, 'name' => $id])->save();
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => $fieldName,
      'entity_type' => 'node',
      'type' => 'boolean',
    ]);
    $fieldStorage->save();
    FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => $id,
      'label' => $id,
    ])->save();
    /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface $display */
    $entityFormId = implode('.', ['node', $id, 'default']);
    $display = EntityFormDisplay::load($entityFormId);
    // The default is boolean_checkbox so if this matches then edpet
    // works.
    $this->assertSame('options_buttons', $display->getComponent($fieldName)['type']);
    // Check it's the load hook which sets the config.
    $this->assertSame('edpet_see_third_party_settings', $this->config("core.entity_form_display.node.$id.default")
      ->get("content.$fieldName.type"));
    $this->assertSame([$fieldName => $edpetName], $display->getThirdPartySettings('edpet'));
    $sync = $this->container->get('config.storage.sync');
    $this->copyConfig($this->container->get('config.storage'), $sync);
    $path = $sync->getFilePath('core.entity_form_display.' . $entityFormId);
    $data = Yaml::decode(file_get_contents($path));
    $this->assertSame('edpet.form.node.default', $data['third_party_settings']['edpet'][$fieldName]);
    $this->assertSame([
      'type' => 'edpet_see_third_party_settings',
      'weight' => 0,
      'settings' => [],
      'third_party_settings' => [],
    ], $data['content'][$fieldName]);
    $data['content']['title']['settings']['size'] += 10;
    $size = $data['content']['title']['settings']['size'];
    file_put_contents($path, Yaml::encode($data));
    $this->configImporter()->reset()->import();
    /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface $entity */
    $entity = EntityFormDisplay::load($entityFormId);
    $this->assertSame($size, $entity->getComponent('title')['settings']['size']);
    // The default is boolean_checkbox so if this matches then edpet
    // works.
    $this->assertSame('options_buttons', $display->getComponent($fieldName)['type']);
    // Check it's the load hook which sets the config.
    $this->assertSame('edpet_see_third_party_settings', $this->config("core.entity_form_display.node.$id.default")
      ->get("content.$fieldName.type"));
  }


}
