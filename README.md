Tthe module uses a separate file to store formatter/widget 
configuration and allows referencing it from entity_view_mode and 
entity_form_mode entities. This separation is completely transparent
to the rest of the system, for example it's possible to edit the 
configuration from the field UI as normal. New bundles will 
automatically inherit the configuration from the same 
entity type and view modee -- this is where the module name comes
from: Entity Display Per Entity Type.

There's no UI yet for setting up, it requires manual editing of the relevant
configuration entity by adding third_party_settings to it. For example, open
`core.entity_view_display.node.video.default.yml` and add:

third_party_settings:
  edpet:
    field_video_mp4: edpet.view.node.default


If you now use the Manage display screen to configure your formatter then it
will be automatically moved to the edpet.view.node.default config object. When
the `core.entity_view_display.node.video.default` entity loads then in memory
the contents of this edpet config object will be mixed in.

Adding the field_video_mp4 field to other node types now will automatically
copy this third party setting to them, automatically sharing the configuration
between them.

Bonus: if a confguration in a file like `edpet.view.node.default.yml` does not 
have a type key, it will automatically hide the  formatter/widget everywhere
it is referenced.
