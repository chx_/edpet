<?php

namespace Drupal\edpet\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'edpet' widget.
 *
 * This class is not a real widget, edpet_entity_form_display_load()
 * replaces it with the real config. This is only here so the exported config
 * can contain type: edpet_see_third_party_settings and empty settings.
 *
 * @FieldWidget(
 *   id = "edpet_see_third_party_settings",
 *   field_types = { }
 * )
 */
class EdpetWidget extends WidgetBase {

  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    throw new \LogicException();
  }
}
