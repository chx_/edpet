<?php

namespace Drupal\edpet\Plugin\Field\FieldFormatter;


use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'edpet_see_third_party_settings' formatter.
 *
 * This class is not a real formatter, edpet_entity_view_display_load()
 * replaces it with the real config. This is only here so the exported config
 * can contain type: edpet_see_third_party_settings and empty settings.
 *
 * @FieldFormatter(
 *   id = "edpet_see_third_party_settings",
 *   field_types = { }
 * )
 */
class EdpetFormatter extends FormatterBase {

  public function viewElements(FieldItemListInterface $items, $langcode) {
    throw new \LogicException();
  }
}
